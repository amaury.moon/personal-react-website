import React from 'react';
import img4 from '../images/img4.png';

function Contact() {
    return (
        <div>
            <h1>This is the contact page</h1>
            <div className='contact'>
                <img className='picture' src={img4} alt='Page under construction...' />
                <h2>This page is still under construction...</h2>
            </div>
        </div>
    )
}

export default Contact;