import React from 'react';
import { useRouteError } from "react-router-dom";

export default function Error() {
    const error = useRouteError();

    return (
        <div className="page">
            <div className="error">
                <h1>Error!</h1>
                <p>Sorry, an unexpected error has occurred.</p>
                <p>
                    <i>{error.statusText || error.message}</i>
                </p>
            </div>
        </div>
    );
}