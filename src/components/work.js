import React from 'react';
import img1 from '../images/img1.png';
import img2 from '../images/img2.png';
import git from '../images/git.png';

function Projects({ image, title, text, url }) {
    return (
        <div className="project">
            <div className='image'>
                <img src={image} className='image' alt='' />
            </div>
            <div className='content'>
                <h2>{title}</h2>
                {text}
                <div className='git'>
                    <a className='button' href={url}>
                        <img src={git} alt='git logo' />
                        Look over the code!
                    </a>
                </div>
            </div>
        </div>
    );
}

function Work() {
    return (
        <div>
            <h1>Projects that I'm currently working on.</h1>
            <Projects
                image={img1}
                title="Personal website in React"
                text="This project is a simple website learn React. It aims to serve as support for the E4 test. I'm still working on to impove the design and make it responsible."
                url="https://gitlab.com/amaury.moon/personal-react-website"
            />
            <Projects
                image={img2}
                title="Medical back-front CRM."
                text="This project is a medical CRM allowing the patient to follow their health care pathway. It's develop on the back front architecture using django and DRF as backend and React as frontend. That project is still under development and doesn't work yet."
                url="https://gitlab.com/amaury.moon/HealthNorthCRM"
            />
        </div>
    );
}

export default Work;
