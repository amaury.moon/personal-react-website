import React from 'react';
import img3 from '../images/img3.png';

function About() {
    return (
        <div>
            <h1>This is page is about me.</h1>
            <div className='about'>
                <div>
                    <h2>Hello world!</h2>
                    <p>My name is Amaury Desnyder. After 10 years working in the IT as project manager, business engineer and then IT systems manager, I'm now a computer science learner.</p>
                </div>
                <img className='picture' src={img3} alt='About me' />
            </div>
        </div>
    );
}

export default About;