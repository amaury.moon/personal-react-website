import React, { Component } from "react";
import { NavLink, Outlet } from "react-router-dom";

function Header() {
  function Logo() {
    return (
      <a href="#default" className="logo">Amaury Desnyder</a>
    );
  }
  function Link({ title, url }) {
    return (
      <NavLink
        to={url}
        className={({ isActive }) =>
          isActive ? "active" : null
        }
      >
        {title}
      </NavLink>
    );
  }
  return (
    <div className="header">
      <Logo />
      <nav className="navigation">
        <Link
          title="About me"
          url="about"
        />
        <Link
          title="Projects"
          url="work"
        />
        <Link
          title="Contact"
          url="contact"
        />
      </nav>
    </div>
  );
}

class App extends Component {
  render() {
    return (
      <>
        <Header />
        <div className="page">
          <div className='content'>
            <Outlet />
          </div>
        </div>
      </>
    );
  }
}

export default App;